# PeciaSync

App(s) to sync files between local and remote locations.  Initial focus will be to sync between Google Drive and local files.  It will support:
1. Individual files.
2. Folder contents, including subfolders/files.
3. Recieving a request another app (should deal with duplicate requests; also, probably, have PeciaSync define location & pass path back to requesting app).
